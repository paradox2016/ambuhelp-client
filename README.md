### install ionic
>npm install -g cordova ionic

### build project
>npm install

>bower install

### web preview
> ionic serve --labs


### build for android
>ionic hooks add

>ionic platform add android`

>ionic state restore --plugins

>ionic resources

>cordova prepare

>ionic build android

**generated apk path**: `platforms/android/build/outputs/apk/android-debug.apk
`

## Test Credentials 

>**email**: test@ambuhelp.com

>**password**: ambuhelp


## Latest APK file 

[app link for wipro](https://wipro365-my.sharepoint.com/personal/sa342798_wipro_com/_layouts/15/guestaccess.aspx?guestaccesstoken=xhgV%2fab4HSpyOw6objxdOUTwGb8041LlaFrSmxPm7ME%3d&docid=2_1a6b4f7e59cae44339d1efd1b5134d1a1&rev=1)

[app link for outside](https://drive.google.com/file/d/0B-_1rYo4xecIYThCb0dQTHVjM00/view?usp=sharing)