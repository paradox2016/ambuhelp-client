app.controller('medicalDetailsCtrl', function ($scope, $rootScope, $stateParams,UserModel, MedicalDetail) {
    
    
    $scope.medicalDetails = $rootScope.user.medicalDetails;
    
    $scope.saveMedicalDetails =  function(){
        
        $rootScope.show("Updating Medical Details");
        UserModel.medicalDetails.update({id:$rootScope.user.id},$scope.medicalDetails).$promise.then(function(res){
            $rootScope.user.medicalDetails = $scope.medicalDetails;
            $rootScope.hide();
            
        },function(err){
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message,err);
        });

    };
    

});
