app.controller('profileCtrl', function ($scope,$rootScope, $stateParams, UserModel, UserDetails) {
    
    $scope.dbUserDetailsCopy  = null;
    $scope.userDetails=$rootScope.user.userDetails;

    $scope.editMode = false;

    $scope.openEditMode = function () {
        $scope.editMode = true;
    };

    $scope.saveEditItems = function () {
        
        // Logic to update data in DB
        $rootScope.show("Updating User Details");
        UserModel.userDetails.update({id:$rootScope.user.id},$scope.userDetails).$promise.then(function(res){
            
            $scope.dbUserDetailsCopy = angular.copy($scope.userDetails);
            $rootScope.user.userDetails = $scope.userDetails;
            $scope.editMode = false;
            $rootScope.hide();
            
        },function(err){
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message,err);
        })
        
    };
    
    $scope.cancelEditMode = function(){
         $scope.userDetails = angular.copy($scope.dbUserDetailsCopy);
         $scope.editMode = false;
    };
    

});