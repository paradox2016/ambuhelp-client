app.controller('registerCtrl', function ($scope, $rootScope, $state, $stateParams, $location,
    $ionicPopup, $ionicLoading, ImageUploadFactory,
    UserModel, UserDetails, MedicalDetail, LoopBackAuth, LoopBackResource) {

    $scope.profilePicImage = "img/avatarmm.png";

    $scope.profilePicImageName = "";
    $scope.registration = {
        "userAvatar": "img/avatarmm.png"
    };

    var defaultMedicalDetails = {
        "diabetes": false,
        "heartDisease": false,
        "hiv": false,
        "cancer": false,
        "highBP": false,
        "lowBP": false,
        "previousSurgery": false,
        "knownAllergies": false
    };
    var defaultUserDetails = {
        "age": 0,
        "height": 0,
        "weight": 0,
        "gender": "",
        "bloodGroup": "",
        "address": ""
    };
    
     $scope.imageUpload = function (event) {
        var imageFile = event.target.files[0]; //FileList object

        $scope.profilePicImageName = imageFile.name;

        var reader = new FileReader();
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(imageFile);

    }

    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.profilePicImage = e.target.result;
            ImageUploadFactory.uploadImage($scope.profilePicImage).then(function (response) {

                $scope.registration.userAvatar = response.url;
            });
        });
    }



    /**
     * @name register()
     * @desctiption
     * register a new user and login
     */
    $scope.register = function () {
        //Display loading template
        $rootScope.show('Registering User!');

        $scope.registration.created = new Date().toJSON();
        $scope.user = UserModel.create($scope.registration).$promise.then(function (res) {

            /**
             * First Login
             */
            UserModel.login({rememberMe: true}, $scope.registration).$promise.then(function (res) {
                $rootScope.hide();
                $rootScope.show('Logging in now!');
                UserModel.userDetails.create({id: LoopBackAuth.currentUserId}, defaultUserDetails);
                UserModel.medicalDetails.create({id: LoopBackAuth.currentUserId}, defaultMedicalDetails);

                UserModel.findOne({
                    filter: {
                        where: {
                            id: LoopBackAuth.currentUserId
                        },
                        include: ["userDetails", "emergencyContacts", "medicalDetails"]
                    }
                }).$promise.then(function (user) {
                    $rootScope.user = user;
                    $rootScope.hide();
                    $state.go('menu.home');
                }, function (err) {
                    $rootScope.hide();
                    $rootScope.showAlert(err.statusText, err.data.error.message, err);
                });


            }, function (err) {
                $scope.hide();
                $rootScope.showAlert(err.statusText, err.data.error.message, err);
            })
        }, function (err) {
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message, err);
        })

    };

})