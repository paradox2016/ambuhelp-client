app.controller('splashCtrl', function ($scope, $rootScope, $state, $stateParams, UserModel, LoopBackAuth, LoopBackResource) {
    if (LoopBackAuth.currentUserId !== null) {
        $rootScope.show("Already logged in. Redirecting!");
        UserModel.findOne({
            filter: {
                where: {
                    id: LoopBackAuth.currentUserId
                },
                include: ["userDetails","emergencyContacts","medicalDetails"]
            }
        }).$promise.then(function (user) {
            $rootScope.user = user;
            $rootScope.hide();
            $state.go('menu.home');
        }, function (err) {
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message, err);
        });
    }
})