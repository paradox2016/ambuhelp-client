app.controller('emergencyContactsCtrl', function ($scope, $rootScope, $stateParams, $ionicModal, ImageUploadFactory, UserModel, EmergencyContact) {

    $scope.emergencyContacts = $rootScope.user.emergencyContacts;

    // template for emergency contact
    var newEmergencyContact = {
        "name": "",
        "phoneNumber": "",
        "contactRelation": "",
        "contactAvatarUrl": "img/avatarmm.png"
    };
    
    var templateValues = {
        create:'templates/emergencyContacts/create-contact-modal.html',
        edit:'templates/emergencyContacts/edit-contact-modal.html'
    }

    $scope.currentContact = {};

    $scope.contactUserAvatar = "img/avatarmm.png";


    $scope.createModal = function (templateType, contact) {

        if (contact !== null) {
            $scope.currentContact = angular.copy(contact);
        } else {
            $scope.currentContact = angular.copy(newEmergencyContact);
        }
        $scope.contactUserAvatar = angular.copy($scope.currentContact.contactAvatarUrl);
        $scope.contactUserImageName = null;
        $ionicModal.fromTemplateUrl(templateValues[templateType], {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
            $scope.openModal();
        });
    };

    $scope.openModal = function () {
        $scope.modal.show();
    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };


    $scope.imageUpload = function (event) {
        var imageFile = event.target.files[0]; //FileList object

        $scope.contactUserImageName = imageFile.name;

        var reader = new FileReader();
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(imageFile);

    }

    $scope.imageIsLoaded = function (e) {
        $scope.$apply(function () {
            $scope.contactUserAvatar = e.target.result;
            ImageUploadFactory.uploadImage($scope.contactUserAvatar).then(function (response) {

                $scope.currentContact.contactAvatarUrl = response.url;
                //console.log(response);
            }, function (err) {
                console.log(err);
            });
        });
    }

    $scope.updateContact = function (contact) {
        $rootScope.show("Updating Emergency Contact");
        EmergencyContact.update({
            where: {
                id: contact.id
            }
        }, contact).$promise.then(function (contact) {
            $rootScope.hide();
            //$scope.emergencyContacts.push(contact);
            $scope.closeModal();
        }, function (err) {
            console.log(err);
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message, err);
            $scope.closeModal();

        });
    };

    $scope.createNewContact = function (contact) {
        console.log(contact);
        $rootScope.show("Adding Emergency Contact");
        UserModel.emergencyContacts.create({
            id: $rootScope.user.id
        }, contact).$promise.then(function (newContact) {
            $rootScope.hide();
            $scope.emergencyContacts.push(contact);
            $scope.closeModal();
        }, function (err) {
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message, err);
            $scope.closeModal();

        });
    };



    $scope.deleteContact = function (contact) {

        $rootScope.showConfirm('Delete Contact', 'Are you sure you want to delete this contact?').then(function (res) {
            if (res) {
                $rootScope.show("Deleting Emergency Contact");
                EmergencyContact.removeById({
                    id: contact.id
                }).$promise.then(function (success) {
                    $rootScope.hide();
                    angular.forEach($scope.emergencyContacts, function (value, index) {
                        if (value.id === contact.id) {
                            $scope.emergencyContacts.splice(index, 1);
                        }
                    });
                    //$scope.emergencyContacts.push(contact);
                    $scope.closeModal();
                }, function (err) {
                    console.log(err);
                    $rootScope.hide();
                    $rootScope.showAlert(err.statusText, err.data.error.message, err);
                    $scope.closeModal();

                });

            }
        })

    };


});
