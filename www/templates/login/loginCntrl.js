app.controller('loginCtrl', function ($scope, $rootScope, $state, $stateParams, $location, $ionicPopup, $ionicLoading,
    UserModel, LoopBackAuth, LoopBackResource) {

    $scope.credentials = {};

    $scope.login = function () {
        $rootScope.show('Logging in!');
        $scope.loginResult = UserModel.login({rememberMe: true}, $scope.credentials)
            .$promise.then(function (res) {
                UserModel.findOne({
                    filter: {
                        where: {
                            id: LoopBackAuth.currentUserId
                        },
                        include: ["userDetails", "emergencyContacts", "medicalDetails"]
                    }
                }).$promise.then(function (user) {
                    $rootScope.user = user;
                    $rootScope.hide();
                    $state.go('menu.home');
                }, function (err) {
                    $rootScope.hide();
                    $rootScope.showAlert(err.statusText, err.data.error.message, err);
                });

            }, function (err) {
                $rootScope.hide();
                $rootScope.showAlert(err.statusText, err.data.error.message, err);
            });
    };

})