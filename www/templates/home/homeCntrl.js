app.controller('homeCtrl', function ($scope, $rootScope,$state,$stateParams, LoopBackAuth, 
                                      $cordovaGeolocation, $cordovaSms, toaster, $http, $ionicLoading, $ionicBackdrop, $ionicPlatform, $timeout,
                                      $interval, MapUtility,RotateIconUtil, HospitalDetail,SOSRequestController,socket, PubSub) {
    
    
    var mapIcon = {
        userIcon: 'img/map_marker.png',
        hospitalIcon: 'img/hospital_map_icon2.png',
        hospitalBuildingIcon: 'img/hospital_map_icon.png',
        ambulanceIcon: 'img/ambulance_map_marker.png',
        ambulanceTopIcon:'img/ambulance_top_map_marker.png'
    }
    
    var markerList = [];
    
    /* Load Ambulance Icon */
    var ambulanceImage =  new Image();
    ambulanceImage.onload = function(){
        console.log("image loaded");
    }
    ambulanceImage.src = mapIcon.ambulanceTopIcon;
    
    // TODO
    $scope.sosRequestModal = {};
    
    $scope.hospitalApproxTime = '--';
    $scope.ambulanceApproxTime = '--';
    /* Show Ionic Loading */
    $rootScope.show('Acquiring location!');
    
    var mapDiv = document.getElementById("map");

    var posOptions = {
        enableHighAccuracy: true,
        timeout: 20000
    };

    var watchOptions = {
        timeout : 3000,
        enableHighAccuracy: false // may cause errors if true
    };


    

    var userMarker = null;
    var cityCircle = null;
    
    $scope.$on('$ionicView.enter', function () {
        // Code you want executed every time view is opened

        if ($rootScope.allowNextLogin) {
            $rootScope.allowNextLogin = false;
            socket.connect();
            $scope.resyncMap();
        }
    });
    
    
   
    
    /* Countdown function */
    $scope.callbackTimer = {
        "cb": null,
        "startTimer": function () {
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
        },
        "stopTimer": function () {
            $scope.$broadcast('timer-stop');
            $scope.timerRunning = false;
            this.reset();
        },
        "finished": function () {
            $scope.timerRunning = false;
            this.cb();
            this.reset();
            $scope.$apply();
        },
        "reset": function () {
            $scope.$broadcast('timer-reset');
        }
    }

    
    
    
    var sosButtonActionSuccessFunction = function(){
        //sendSMSTOEmergencyContacts($rootScope.user.name, $scope.currentPosition);
        var user = $rootScope.user;
        user.location ={
            lat : $scope.currentPosition.lat(),
            lng : $scope.currentPosition.lng()
        }
        $rootScope.show("Finding help for you...");
        SOSRequestController.raiseSOSRequest(user).$promise.then(
        function(bookingRequest){
            $rootScope.hide();

           // $scope.sosRequestModal = bookingRequest;

           $state.go('menu.sosHome',{sosRequest: bookingRequest});
        },function(err){
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message, err);
        })
        
    }
    
    /*SOS Button Function*/
    $scope.sosButtonAction = function () {
        
        $scope.callbackTimer.cb = sosButtonActionSuccessFunction;
        $scope.callbackTimer.startTimer();
    };
    

    $scope.cancelSOSButtonAction = function () {
        $scope.callbackTimer.stopTimer();
    }
    
    /* MAP Functions */
    
    var initMap = function(){
        $scope.currentPosition = new google.maps.LatLng(12.9538477,77.3507442);
        var bangaloreCenter =  new google.maps.LatLng(12.9538477,77.3507442);
        var mapOptions = MapUtility.getDefaultMapOption(bangaloreCenter);
        $scope.map = new google.maps.Map(mapDiv, mapOptions);
    }
    
    var findMinTimeLocation =function(elements) {
        var minTime = 24 * 3600;
        var minTimeText = "--";
        
        angular.forEach(elements,function(element,index){
            if (minTime > element.duration.value) {
                minTime = element.duration.value;
                minTimeText = element.duration.text;
            }
        })
    
        return minTimeText;
    }
    
    var clearMarkers = function(){
        angular.forEach(markerList,function(marker){
            marker.setMap(null);
        })
    }
    
    
    /* Method to load Ambulance Markers for radius of 5 KM*/
    var loadAmbulanceMarkers =function(coords){
        
        SOSRequestController.findNearByAmbulance({
            coordinates:coords
        }).$promise.then(function (res) {
            var destinationCoords = [];
            angular.forEach(res.nearByAmbulance, function (ambulance, index) {

                var markerPos = new google.maps.LatLng(ambulance.location.lat, ambulance.location.lng);
                destinationCoords.push(markerPos);

                var ambulanceMarker = RotateIconUtil.putRotateMarkerIcon($scope.map,markerPos,ambulanceImage,32,Math.floor(Math.random() * 360));
                markerList.push(ambulanceMarker);
            });
            
            MapUtility.findPathsFromOriginToDestination($scope.currentPosition, destinationCoords, function (err, ambulancePath) {
                if (err) {
                    console.log(err)
                } else {
                    $scope.ambulanceApproxTime = findMinTimeLocation(ambulancePath);
                    $scope.$apply();
                }
            });
        },function(err){
            console.log(err);       
        })
    }
    
    
    /* Method to load Hospital Markser for area of 5 KM */
    var loadHospitalMarkers = function(coords) {
        HospitalDetail.findNearByHospitals({
            coordinates: coords
        }).$promise.then(function (res) {

            var destinationCoords = [];
            angular.forEach(res.nearbyHospitals, function (hospital, index) {

                var markerPos = new google.maps.LatLng(hospital.mapLocation.lat, hospital.mapLocation.lng);
                destinationCoords.push(markerPos);

                var hopsitalMarker = MapUtility.putMarker($scope.map, markerPos, MapUtility.getMapIcon(mapIcon.hospitalIcon, 32));
                markerList.push(hopsitalMarker);
                MapUtility.addInfoWindow($scope.map, hopsitalMarker, hospital.name);
                

            });


            MapUtility.findPathsFromOriginToDestination($scope.currentPosition, destinationCoords, function (err, hospitalPath) {
                if (err) {
                    console.log(err)
                } else {
                    $scope.hospitalApproxTime = findMinTimeLocation(hospitalPath);
                    $scope.$apply();
                }
            });

        }, function (err) {
            console.log(err);
        })
    }
    
    $scope.moveToCenter = function () {
        $scope.map.panTo($scope.currentPosition)
    }

    
    
    var putCurrentPositionOnMap = function(isResync){
        
        var options = isResync ? watchOptions : posOptions;
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
             $scope.currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
             if($scope.map === null){
                 initMap();
             }
             $scope.moveToCenter();
             if(userMarker!==null){
                 userMarker.setMap(null)
             }
             userMarker = MapUtility.putMarker($scope.map, $scope.currentPosition, MapUtility.getMapIcon(mapIcon.userIcon, 48));
             
             if(cityCircle!==null){
                 cityCircle.setMap(null);
             }
             cityCircle = MapUtility.setMapCircle($scope.map, $scope.currentPosition, 5);
             clearMarkers();
             loadHospitalMarkers($scope.currentPosition);
             loadAmbulanceMarkers($scope.currentPosition);
            
             
             
             /* Hide Ionic Loading */
             $rootScope.hide();

        }, function (err) {
            /* Hide Ionic Loading */
            $rootScope.hide();
            console.log(err);
        });
    }
    
    $scope.resyncMap = function(){
        $rootScope.show('resyncing location!');
        putCurrentPositionOnMap(true);
    }

    $ionicPlatform.ready(function () {
        initMap();
        putCurrentPositionOnMap(false);
        
    });
    
    
    
    
    var syncLocationWithSocket = function(){
        if($scope.currentPosition){
            /* EMIT current location*/
             console.log('socket sync called');
             var location = {
                lat:$scope.currentPosition.lat(),
                lng:$scope.currentPosition.lng()
             }
            socket.emit('update-location',{id:LoopBackAuth.currentUserId, userType:'user',location:location});
        } else{
            console.log('waiting for current position');
        }
    }
    
    $rootScope.$watch('isSocketConnected',function(){
        if($rootScope.isSocketConnected){
            syncLocationWithSocket();
        } else{
            console.log('waiting for socket connect');
        }
    });
    
     $scope.$watch('currentPosition',function(){
        syncLocationWithSocket();
     });
    
   
    

})
