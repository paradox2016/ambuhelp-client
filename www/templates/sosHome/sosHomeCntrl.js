app.controller('sosHomeCtrl', function ($scope, $rootScope, $state, $stateParams, $cordovaGeolocation, $cordovaSms,
    toaster, $http, $ionicLoading, $ionicBackdrop, $ionicPlatform, $timeout, $interval, MapUtility, HospitalDetail, 
    socket, PubSub,reverseGeocode) {

    $scope.sosRequest = $stateParams.sosRequest.bookingRequest;
    
    var locationNotificationMsg = function (name, address, pos) {
        
        return  'I am in emergency.' +
                '\nEmergency Location '+ address +
                '\nLink: http://maps.google.com/?q=' + pos.lat() + ',' + pos.lng() +
                '\n-' + name + ' via Ambuhelp';
    }
    
    var infoNotificationMsg = function(name,ambulance,hospital){
        return 'Driver and Hospital assigned ( Booking ID: '+$scope.sosRequest.id+' )' +
                '\n Driver :'+ambulance.driverDetails.name+' ( '+ambulance.driverDetails.phoneNumber +' ) '+
                '\n Ambulance : '+ambulance.username+
                '\n Hospital: '+ hospital.hospitalDetails.name+ ', '+ hospital.hospitalDetails.address +' ( '+hospital.hospitalDetails.phoneNumber[0] +' ) '+
                '\n Link: http://maps.google.com/?q='+hospital.hospitalDetails.mapLocation.lat + ',' + hospital.hospitalDetails.mapLocation.lng +
                '\n-' + name + ' via Ambuhelp';
    }
    
    var sendSMS = function(msg){
        
        var options = {
            android: {
                intent: '' // send SMS without open any other app
            }
        };
        
        angular.forEach($rootScope.user.emergencyContacts, function (contact, index) {
            $cordovaSms
                .send(contact.phoneNumber, msg, options)
                .then(function (succes) {
                    toaster.pop({
                        type: 'success',
                        title: "SMS Notification",
                        body: contact.name + " Notified about emergency"
                    });
                }, function (error) {
                    console.log('sms send failed to ' + contact.name);
                });
            })
    }
    
     /* Send MSG TO emergency Contacts*/
    var sendSMSTOEmergencyContacts = function (name, pos) {
        reverseGeocode.geocodePosition(pos.lat(), pos.lng(), function(address){
            sendSMS(locationNotificationMsg(name,address, pos));
        })
    }
    
    var sendSOSInfo = function(sos){
        sendSMS(infoNotificationMsg(sos.user.name,sos.ambulance,sos.hospital));
    }

    // Ambulance Location
    $scope.currentDestination;
    $scope.currentPosition;

    $scope.isPicked = false;

    var currentPositionMarker;
    var currentDestinationMarker;

    if (!$scope.sosRequest.hospital.logo) {
        $scope.sosRequest.hospital.logo = "img/hospital-image.png";
    }
    var mapDiv = document.getElementById("bookingMap");
    var posOptions = {
        enableHighAccuracy: true,
        timeout: 20000
    };


    var watchOptions = {
        timeout: 3000,
        enableHighAccuracy: false // may cause errors if true
    };

    var mapIcon = {
        userIcon: 'img/map_marker.png',
        hospitalIcon: 'img/hospital_map_icon.png',
        ambulanceIcon: 'img/ambulance_map_marker.png'
    }

    var pathProperties = new google.maps.Polyline({
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 5
    });

    var bounds = null;

    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer({
        polylineOptions: pathProperties
    });
    directionsRenderer.setOptions({
        suppressMarkers: true
    });

    var getDirectionRequest = function (origin, destination) {
        return {
            origin: origin,
            destination: destination,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        }
    }

    var calculateDuration = function (routes, bookingDetail) {
        bookingDetail.route = {
            parts: [],
            totalDuration: 0
        }
        angular.forEach(routes, function (route, index) {
            bookingDetail.route.parts.push(route.duration);
            bookingDetail.route.totalDuration += route.duration.value;
        });


    }

    var getDuration = function (bookingDetail, isPicked) {

        var request = {
            destination: new google.maps.LatLng(bookingDetail.hospital.hospitalDetails.mapLocation.lat, bookingDetail.hospital.hospitalDetails.mapLocation.lng),
            waypoints: [],
            unitSystem: google.maps.UnitSystem.METRIC,
            travelMode: google.maps.TravelMode.DRIVING
        };

        if (isPicked) {
            request.origin = new google.maps.LatLng(bookingDetail.user.location.lat, bookingDetail.user.location.lng);
        } else {
            request.origin = new google.maps.LatLng(bookingDetail.ambulance.location.lat, bookingDetail.ambulance.location.lng);
            request.waypoints.push({
                location: new google.maps.LatLng(bookingDetail.user.location.lat, bookingDetail.user.location.lng),
                stopover: true
            })
        }

        directionsService.route(request, function (result, status) {
            if (status == 'OK') {
                calculateDuration(result.routes[0].legs, bookingDetail);
            }
        });

    }


    var renderDirections = function () {

        directionsService.route(getDirectionRequest($scope.currentPosition, $scope.currentDestination), function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRenderer.setDirections(response);
            }
        });

    }

    var putMarkers = function (isPicked) {

        if (isPicked) {
            currentDestinationMarker.setIcon(MapUtility.getMapIcon(mapIcon.hospitalIcon, 36));
            currentDestinationMarker.setPosition($scope.currentDestination);
        } else {
            currentPositionMarker = MapUtility.putMarker($scope.map, $scope.currentPosition,
                MapUtility.getMapIcon(mapIcon.ambulanceIcon, 36));;
            currentDestinationMarker = MapUtility.putMarker($scope.map, $scope.currentDestination,
                MapUtility.getMapIcon(mapIcon.userIcon, 48));
        }

    }


    var reCalibrateBound = function () {

        var bounds = new google.maps.LatLngBounds();
        bounds.extend($scope.currentPosition);
        bounds.extend($scope.currentDestination);

        $scope.map.setCenter(bounds.getCenter());
        $scope.map.fitBounds(bounds);

        //remove one zoom level to ensure no marker is on the edge.
        $scope.map.setZoom($scope.map.getZoom() - 1);

        // set a minimum zoom 
        // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
        if ($scope.map.getZoom() > 15) {
            $scope.map.setZoom(15);
        }

    }



    $scope.initMap = function () {
        $scope.currentDestination = new google.maps.LatLng($scope.sosRequest.user.location.lat, $scope.sosRequest.user.location.lng);
        $scope.currentPosition = new google.maps.LatLng($scope.sosRequest.ambulance.location.lat, $scope.sosRequest.ambulance.location.lng);
        var mapOptions = MapUtility.getDefaultMapOption($scope.currentDestination);
        $scope.map = new google.maps.Map(mapDiv, mapOptions);
        directionsRenderer.setMap($scope.map);

        reCalibrateBound();
        putMarkers($scope.isPicked);
        renderDirections();
        
        sendSMSTOEmergencyContacts($scope.sosRequest.user.name,$scope.currentDestination);
        
        // Send Driver and Ambulance Contact
        $timeout(function(){
            sendSOSInfo($scope.sosRequest);
        },15000);

    };


    $ionicPlatform.ready(function () {
        $scope.initMap();
    });



    PubSub.subscribe({
        collectionName: 'LocationUpdateServer',
        method: 'PATCH',
        modelId: $scope.sosRequest.id
    }, function (driverLocation) {

        $scope.currentPosition = new google.maps.LatLng(driverLocation.lat, driverLocation.lng);
        currentPositionMarker.setPosition($scope.currentPosition);

        getDuration($scope.sosRequest, $scope.isPicked);
        renderDirections();

    });

    PubSub.subscribe({
        collectionName: 'SOSPickupUpdate',
        method: 'UPDATE',
        modelId: $scope.sosRequest.id
    }, function (data) {
        $scope.currentDestination = new google.maps.LatLng($scope.sosRequest.hospital.hospitalDetails.mapLocation.lat, $scope.sosRequest.hospital.hospitalDetails.mapLocation.lng);
        $scope.isPicked = true;
        putMarkers($scope.isPicked);
        renderDirections();
        sendSMS('Ambulance pickup done. '+
                'Taking '+$scope.sosRequest.user.name+ ' to '+$scope.sosRequest.hospital.hospitalDetails.name +
                '\n -via Ambuhelp');

    });

    PubSub.subscribe({
        collectionName: 'SOSRequestComplete',
        method: 'UPDATE',
        modelId: $scope.sosRequest.id
    }, function (data) {
        sendSMS('Ambulance drop done. '+
                $scope.sosRequest.user.name+ ' is admitted to '+$scope.sosRequest.hospital.hospitalDetails.name +
                '\n -via Ambuhelp');
        $scope.map = null;
        $state.go('menu.home');
    });

});