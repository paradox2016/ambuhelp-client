app.controller('menuCtrl', function ($scope, $rootScope, $state, $stateParams, UserModel,socket) {
    $scope.logout = function () {
        $rootScope.user = null;
        socket.disconnect();
        $rootScope.show('Logging Out... Please Wait');
        $rootScope.allowNextLogin = true;
        UserModel.logout(function () {
            $rootScope.hide();
            $state.go('splash');
        });
    }


})
