angular.module('app.directives', [])

.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                        console.log(scope.profilePicImageName);
                        scope.profilePicImageName = changeEvent.target.files[0].name;

                        scope.$apply(function () {
                            scope.profilePicImageName = changeEvent.target.files[0].name;
                        })


                        console.log(scope.profilePicImageName);
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);

            });
        }
    }
}]);