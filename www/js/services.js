angular.module('app.services', [])
    .factory('ImageUploadFactory', function ($q, $ionicLoading, $ionicPlatform, $cordovaFileTransfer) {
        return {
            uploadImage: function (imageURI) {
                var CloudinaryConfigs = {
                    "upload_preset": "rqhcbucd",
                    "api_url": "https://api.cloudinary.com/v1_1/dren4jgbp/image/upload"
                }
                console.log('start upload image.');
                var deferred = $q.defer();

                uploadFile();


                function uploadFile() {
                    $ionicLoading.show({
                        template: 'Uploading image...'
                    });
                    // Add the Cloudinary "upload preset" name to the headers
                    var uploadOptions = {
                        params: {
                            'upload_preset': CloudinaryConfigs.upload_preset
                        }
                    };
                    $ionicPlatform.ready(function () {
                        $cordovaFileTransfer.upload(CloudinaryConfigs.api_url, imageURI, uploadOptions, false)

                        .then(function (result) {
                            // Let the user know the upload is completed
                            $ionicLoading.show({
                                template: 'Done.',
                                duration: 1000
                            });
                            console.log(result.response);
                            var response = JSON.parse(decodeURIComponent(result.response));
                            deferred.resolve(response);
                        }, function (err) {
                            // Uh oh!
                            console.log(err);
                            $ionicLoading.show({
                                template: 'Failed.',
                                duration: 3000
                            });
                            deferred.reject(err);
                        }, function (progress) {

                        });
                    });

                }
                return deferred.promise;
            }
        }
    });
