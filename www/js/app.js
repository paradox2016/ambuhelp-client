// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('app', ['ionic', 'ngCordova', 'lbServices','app.controllers', 'app.routes', 
                                 'app.directives', 'app.services','map.services', 'timer','toaster', 
                                 'ngAnimate','btford.socket-io','LoopbackSocketIntegration','humanSeconds','angular-input-stars','AngularReverseGeocode'])

.config(function ($ionicConfigProvider) {

})


.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

    });
})

.run(function ($rootScope, $ionicPopup, $ionicLoading) {

    $rootScope.show = function (message) {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>' + message
        });
    };

    //Hide function
    $rootScope.hide = function () {
        $ionicLoading.hide();
    };


    $rootScope.showAlert = function (title, errorMsg, errorObject) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: errorMsg
        });
        alertPopup.then(function () {
            console.log(errorObjects);
        });
    };

    $rootScope.showConfirm = function (title, message) {
        var confirmPopup = $ionicPopup.confirm({
            title: title,
            template: message
        });

        return confirmPopup;
    };
})