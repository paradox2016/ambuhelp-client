angular.module('app.routes', [])

.config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
        .state('splash', {
            url: '/splash',
            templateUrl: 'templates/splash/splash.html',
            controller: 'splashCtrl'
        })

    .state('login', {
        url: '/login',
        templateUrl: 'templates/login/login.html',
        controller: 'loginCtrl'

    })

    .state('register', {
        url: '/register',
        templateUrl: 'templates/register/register.html',
        controller: 'registerCtrl'

    })

    .state('menu', {
        url: '/side-menu21',
        templateUrl: 'templates/menu/menu.html',
        controller: 'menuCtrl'
    })

    .state('menu.home', {
        url: '/home',
        views: {
            'side-menu21': {
                templateUrl: 'templates/home/home.html',
                controller: 'homeCtrl'
            }
        }
    })

    .state('menu.sosHome', {
        url: '/sosHome',
        params:{sosRequest:null},
        views: {
            'side-menu21': {
                templateUrl: 'templates/sosHome/sosHome.html',
                controller: 'sosHomeCtrl'
            }
        }

    })

    .state('menu.profile', {
        url: '/profile',
        views: {
            'side-menu21': {
                templateUrl: 'templates/profile/profile.html',
                controller: 'profileCtrl'
            }
        }
    })



    .state('menu.medicalDetails', {
        url: '/medicalDetails',
        views: {
            'side-menu21': {
                templateUrl: 'templates/medicalDetails/medicalDetails.html',
                controller: 'medicalDetailsCtrl'
            }
        }
    })

    .state('menu.emergencyContacts', {
        url: '/emergencyContacts',
        views: {
            'side-menu21': {
                templateUrl: 'templates/emergencyContacts/emergencyContacts.html',
                controller: 'emergencyContactsCtrl'
            }
        }
    })

    .state('menu.history', {
        url: '/history',
        views: {
            'side-menu21': {
                templateUrl: 'templates/history/history.html',
                controller: 'historyCtrl'
            }
        }
    })



    $urlRouterProvider.otherwise('/splash')



});
